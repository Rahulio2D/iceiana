﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBlockController : MonoBehaviour
{

  public enum Should { APPEAR, DISAPPEAR };
  public Should should;

  public enum Direction { UP, DOWN, LEFT, RIGHT };
  public Direction direction;
  private Vector2 dir;

  public LayerMask playerLayer;
  private SpriteRenderer sprite;
  private BoxCollider2D box;

  void OnEnable()
  {
    Physics2D.queriesStartInColliders = false;
    dir = GetDirection();
    sprite = GetComponent<SpriteRenderer>();
    box = GetComponent<BoxCollider2D>();
    sprite.enabled = (should != Should.APPEAR);
    box.enabled = (should != Should.APPEAR);
  }

  void Update()
  {
    if (CheckCollision()) ChangeState();
  }

  private Vector2 GetDirection()
  {
    if (direction == Direction.UP) return new Vector2(0, 1);
    if (direction == Direction.DOWN) return new Vector2(0, -1);
    if (direction == Direction.LEFT) return new Vector2(-1, 0);
    if (direction == Direction.RIGHT) return new Vector2(1, 0);
    return Vector2.zero;
  }

  private bool CheckCollision()
  {
    if (Physics2D.Raycast(transform.position, dir, 1.0f, playerLayer))
    {
      if (PlayerController.Instance.GetDir() == GetDirection() * -1) return true;
      return false;
    }
    return false;
  }

  private void ChangeState()
  {
    sprite.enabled = (should == Should.APPEAR);
    box.enabled = (should == Should.APPEAR);
  }
}
