# Iceiana

## Description
Iceiana is a 2D puzzle game that was made in under 72 hours as part of [Ludum Dare 47](https://ldjam.com/events/ludum-dare/47/theme), where the theme was 'Stuck in a loop'. In the game you play as Iceiana, an esteemed adventurer who is searching for the treasure of Nerak. However, each block in the game has the possibility of appearing or disappearing based on the angle at which you approach, causing you to either rework your approach or slide to your doom.

The game was made using Unity so the repository can be easily downloaded and run with any Unity Editor (I recommend Unity 2019.4.11f1 or above), however, if you'd rather play the game as an executable or in your browser, you can easily play the game on [our itch.io page](https://rahulio2d.itch.io/iceiana)

## Controls
To control your character you only need to use the Arrow Keys or the WASD Keys. Additionally, the spacebar is used to navigate through menus.

## Credits
This game was made by:
- [Rahul Sharma](https://www.rahulsharma.uk) - Programmer, Project Management, Artist
- [Fraser Rees](https://www.twitter.com/FineFraser) - Level Design
- [Rupert Cole](https://www.rupertcole.co.uk/) - Sound Design

No other assets were used during development, however, for the WebGL build of the game we utilised the [Better Minimal WebGL Template](https://seansleblanc.itch.io/better-minimal-webgl-template)
